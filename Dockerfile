FROM debian:latest
MAINTAINER Torsten Irländer
# Base packages needed for the ringo installation
RUN apt-get -y update && apt-get -y install python-pip python-psycopg2 python-lxml
# Install extra packages needed to compile python-Levenshtein.
RUN apt-get -y install gcc python-dev
# Install postgres server
RUN apt-get -y install postgresql sudo
# Install latest available ringo packages from PYPI.
RUN pip install --pre \
	ringo \
	ringo_comment \
	ringo_tag
RUN useradd -m --home /home/ringo --shell /bin/bash ringo
ADD setup_database.sh .
CMD ["echo" "Setup database with /setup_database.sh"]
